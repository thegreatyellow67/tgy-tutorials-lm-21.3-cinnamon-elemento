#!/bin/bash

clear

THEME="elemento"
CURSOR_THEME="whitesur"
ICON_THEME="mkos-bigsur"
THEME_FOLDER="lm-21.3-cinnamon-${THEME}"
BASE_PATH="${HOME}/Scaricati/"

echo ""
echo "   ██████╗██╗███╗   ██╗███╗   ██╗ █████╗ ███╗   ███╗ ██████╗ ███╗   ██╗"
echo "  ██╔════╝██║████╗  ██║████╗  ██║██╔══██╗████╗ ████║██╔═══██╗████╗  ██║"
echo "  ██║     ██║██╔██╗ ██║██╔██╗ ██║███████║██╔████╔██║██║   ██║██╔██╗ ██║"
echo "  ██║     ██║██║╚██╗██║██║╚██╗██║██╔══██║██║╚██╔╝██║██║   ██║██║╚██╗██║"
echo "  ╚██████╗██║██║ ╚████║██║ ╚████║██║  ██║██║ ╚═╝ ██║╚██████╔╝██║ ╚████║"
echo "   ╚═════╝╚═╝╚═╝  ╚═══╝╚═╝  ╚═══╝╚═╝  ╚═╝╚═╝     ╚═╝ ╚═════╝ ╚═╝  ╚═══╝"
echo ""                                                                     
echo "  ███████╗██╗     ███████╗███╗   ███╗███████╗███╗   ██╗████████╗ ██████╗"
echo "  ██╔════╝██║     ██╔════╝████╗ ████║██╔════╝████╗  ██║╚══██╔══╝██╔═══██╗"
echo "  █████╗  ██║     █████╗  ██╔████╔██║█████╗  ██╔██╗ ██║   ██║   ██║   ██║"
echo "  ██╔══╝  ██║     ██╔══╝  ██║╚██╔╝██║██╔══╝  ██║╚██╗██║   ██║   ██║   ██║"
echo "  ███████╗███████╗███████╗██║ ╚═╝ ██║███████╗██║ ╚████║   ██║   ╚██████╔╝"
echo "  ╚══════╝╚══════╝╚══════╝╚═╝     ╚═╝╚══════╝╚═╝  ╚═══╝   ╚═╝    ╚═════╝"
echo ""
echo "  ===================================================================="
echo "  Script per automatizzare la copia delle"
echo "  risorse per il tema Cinnamon ${THEME^^}"
echo "  Scritto da TGY-TUTORIALS il 07/03/2024"
echo "  ===================================================================="

function goto
{
    label=$1
    cmd=$(sed -n "/$label:/{:a;n;p;ba};" $0 | grep -v ':$')
    eval "$cmd"
    exit
}

echo ""
echo "  Premi 's' per continuare o 'n' per uscire dallo script..."

# In attesa che l'utente prema un tasto
read -s -n 1 key

# Controlla se è stato premuto un tasto
case $key in
    s|S)
        goto main
        ;;
    n|N)
        echo "  Termino lo script...a presto!"
        exit 1
        ;;
    *)
        echo "  Tasto non valido. Per favore premi 's' o 'n'."
        sleep 5
        exit 1
        ;;
esac

main:

#
# Crea la cartella Scaricati se
# eventualmente mancante
#
if [ ! -d ${BASE_PATH} ];then
  mkdir -p ${BASE_PATH}
fi

cd ${BASE_PATH}

if [ ! -d ${THEME_FOLDER} ];then

  echo ""
  echo "  Sto per scaricare le risorse da GitLab, un pò di pazienza..."
  echo ""

  #
  # Installa git se mancante
  #
  if ! location="$(type -p "git")" || [ -z "git" ]; then
    echo "  Installo git per far funzionare questo script..."
    sudo apt install -y git &> /dev/null
  fi

  git clone https://gitlab.com/thegreatyellow67/tgy-tutorials-lm-21.3-cinnamon-${THEME}.git ${THEME_FOLDER}

  cd ${THEME_FOLDER}
  rm -fr .git

  clear
  echo ""
  echo "  Sto installando alcuni pacchetti utili, un pò di pazienza..."
  echo ""
  sudo sudo apt install fonts-powerline conky-all jq playerctl vlc ffmpeg dconf-editor tilix gpick gimp inkscape htop btop cava gpaste gir1.2-gpaste-1.0 rofi -y &> /dev/null

  #
  # Crea cartelle eventualmente mancanti
  #
  if [ ! -d ~/.fonts ];then
    mkdir -p ~/.fonts
  fi

  if [ ! -d ~/.themes ];then
    mkdir -p ~/.themes
  fi

  if [ ! -d ~/.icons ];then
    mkdir -p ~/.icons
  fi

  if [ ! -d ~/.config/conky ];then
    mkdir -p ~/.config/conky
  fi

  if [ ! -d ~/.config/autostart ];then
    mkdir -p ~/.config/autostart
  fi

  if [ ! -d ~/.local/share/applications ];then
    mkdir -p ~/.local/share/applications
  fi

  if [ ! -d /boot/grub/themes ];then
    sudo mkdir -p /boot/grub/themes
  fi

  clear
  echo ""
  echo "  Installazione dei caratteri..."
  echo ""
  sudo apt install fonts-inter -y &> /dev/null
  cp -r ${THEME}-fonts/* ~/.fonts
  sudo cp -r ${THEME}-fonts/Hack/ /usr/share/fonts/truetype/
  fc-cache -fr
  sudo fc-cache -fr
  sleep 3

  clear
  echo ""
  echo "  Installazione del tema GTK ${THEME^^}, un pò di pazienza..."
  echo ""
  cp -r ${THEME}-gtk-themes/* ~/.themes
  sudo cp -r ${THEME}-gtk-themes/* /usr/share/themes
  sleep 3

  clear
  echo ""
  echo "  Installazione delle icone ${ICON_THEME^^}, un pò di pazienza..."
  echo ""
  cp -r ${THEME}-icons/* ~/.icons
  sudo cp -r ${THEME}-icons/* /usr/share/icons
  sleep 3

  clear
  echo ""
  echo "  Installazione dei cursori ${CURSOR_THEME^^}, un pò di pazienza..."
  echo ""
  cp -r ${THEME}-cursors/* ~/.icons
  sudo cp -r ${THEME}-cursors/* /usr/share/icons
  sleep 3

  clear
  echo ""
  echo "  Installazione delle icone per il Menù..."
  echo ""
  cp -r start-menu-icons/ ~/.icons
  sleep 3

  clear
  echo ""
  echo "  Installazione degli sfondi..."
  echo ""
  cp -r ${THEME}-backgrounds/* ~/Immagini
  sleep 3

  clear
  echo ""
  echo "  Installazione delle azioni per Nemo..."
  echo ""
  cp -r nemo-actions/* ~/.local/share/nemo/actions
  sleep 3

  clear
  echo ""
  echo "  Installazione dello script di Conky..."
  echo ""
  cp -r conky-config/conky/* ~/.config/conky
  cp conky-config/autostart/* ~/.config/autostart
  cp conky-config/applications/* ~/.local/share/applications
  sudo cp conky-config/script/launch-conky /usr/local/bin
  sudo chmod 755 /usr/local/bin/launch-conky
  
  WIFI_DEVICE=`nmcli device | grep wifi\ | awk '{print $1}'`
  LAN_DEVICE=`ip -br l | awk '$1 !~ "lo|vir|wl" { print $1}'`

  # Sostituzione dei nomi dipositivo per
  # il wireless e/o la ethernet  
  if [[ "${WIFI_DEVICE}" != "" ]]; then
    sed -i "s/wlan0/${WIFI_DEVICE}/g" ~/.config/conky/Grumium-Dark/Grumium-Dark.conf
    sed -i "s/wlan0/${WIFI_DEVICE}/g" ~/.config/conky/Grumium-Light/Grumium-Light.conf
  fi

  if [[ "${LAN_DEVICE}" != "" ]]; then
    sed -i "s/enp0s3/${LAN_DEVICE}/g" ~/.config/conky/Grumium-Dark/Grumium-Dark.conf
    sed -i "s/enp0s3/${LAN_DEVICE}/g" ~/.config/conky/Grumium-Light/Grumium-Light.conf
  fi
  sleep 3

  clear
  echo ""
  echo "  Installazione della configurazione di Cava..."
  echo ""
  cp -r cava-config/.config/* ~/.config
  sleep 3

  clear
  echo ""
  echo "  Installazione della configurazione di Glava..."
  echo ""
  cp -r glava-config/.config/glava/ ~/.config
  cp glava-config/.config/autostart/* ~/.config/autostart
  cp glava-config/.local/share/applications/* ~/.local/share/applications
  sudo cp glava-config/script/launch-glava /usr/local/bin
  sudo chmod 755 /usr/local/bin/launch-glava
  sleep 3

  clear
  echo ""
  echo "  Installazione dello sfondo per la finestra di accesso..."
  echo ""
  sudo cp -r login-window/ /usr/share/backgrounds
  sleep 3

  clear
  echo ""
  echo "  Installazione di macchina e relativa configurazione..."
  echo ""
  sudo cp macchina/macchina-linux-x86_64 /usr/local/bin/macchina
  sudo chmod +x /usr/local/bin/macchina
  cp -r macchina/.config/* ~/.config
  sleep 3

  clear
  echo ""
  echo "  Installazione della configurazione di neofetch..."
  echo ""
  cp -r neofetch/ ~/.config
  sleep 3

  clear
  echo ""
  echo "  Installazione del tema per Ulauncher..."
  echo ""
  cp -r ulauncher/ulauncher-theme/.config/* ~/.config
  sleep 3

  clear
  echo ""
  echo "  Installazione della configurazione di Rofi..."
  echo ""
  cp -r rofi-config/.config/* ~/.config
  sleep 3

  clear
  echo ""
  echo "  Installazione tema, applicazioni e configurazione di Plank..."
  echo ""
  sed -i "s/tgy-tutorials/${USER}/g" plank-config/.config/plank/dock1/launchers/plank-prefs.dockitem
  cp -r plank-config/.config/* ~/.config
  cp -r plank-config/.local/share/plank/ ~/.local/share
  cp -r plank-config/.local/share/applications/* ~/.local/share/applications
  sed -i "s/tgy-tutorials/${USER}/g" ~/.local/share/applications/separator.desktop
  sed -i "s/tgy-tutorials/${USER}/g" ~/.local/share/applications/separator1.desktop
  sudo cp plank-config/script/launch-plank /usr/local/bin
  sudo chmod 755 /usr/local/bin/launch-plank
  dconf load /net/launchpad/plank/ < plank-config/plank-settings.conf
  sleep 3

  clear
  echo ""
  echo "  Installazione del tema Sugar Candy per Grub..."
  echo ""
  sudo cp -r sugar-candy/ /boot/grub/themes
  sleep 3

  clear
  echo ""
  echo "  Sostituzione con utente corrente in"
  echo "  alcuni files di configurazione..."
  echo ""
  sed -i "s/tgy-tutorials/${USER}/g" applets-confs/applet-command-launcher-chiara.json
  sed -i "s/tgy-tutorials/${USER}/g" applets-confs/applet-command-launcher-scura.json
  sed -i "s/tgy-tutorials/${USER}/g" applets-confs/applet-gestore-sessione-icona-scura.json
  sed -i "s/tgy-tutorials/${USER}/g" applets-confs/applet-gestore-sessione-icona-chiara.json
  sed -i "s/tgy-tutorials/${USER}/g" applets-confs/applet-menu-icona-chiara.json
  sed -i "s/tgy-tutorials/${USER}/g" applets-confs/applet-menu-icona-scura.json
  sed -i "s/tgy-tutorials/${USER}/g" dconf-settings/keybindings.conf
  sleep 3

  clear
  echo ""
  echo "  Importazione impostazioni di dconf (scorciatoie tastiera)..."
  echo ""
  dconf load /org/cinnamon/desktop/keybindings/ < dconf-settings/keybindings.conf
  sleep 3

  clear
  echo ""
  echo "  Risorse installate con successo!"
  sleep 3
  echo ""

else
  echo "  La cartella ${THEME_FOLDER} esiste! se vuoi scaricare"
  echo "  le risorse nuovamente devi cancellare la cartella e"
  echo "  rieseguire questo script in un terminale. Buona giornata!"
  echo ""
fi
