- Impostare i nomi delle aree di lavoro:
4 aree di lavoro: web, chat, term, media

- Visibilità del pannello:
Pannello a scomparsa automatica: mostra sempre il pannello

- Impostazioni del pannello:
	Altezza del pannello: 28
	Posizione: in alto

	Zona sinistra:
	Dimensione dei caratteri: 9.5pt
	Dimensione delle icone colorate: 22px
	Dimensione delle icone simboliche: 22px

	Zona centrale:
	Dimensione dei caratteri: 9.5pt
	Dimensione delle icone colorate: 22px
	Dimensione delle icone simboliche: 22px
	Zona destra:
	Dimensione dei caratteri: 9.5pt
	Dimensione delle icone colorate: 16px
	Dimensione delle icone simboliche: 16px

- Scaricare le seguenti applet:
	- Command Launcher
	- Gestore Sessione
	- GPaste Reloaded
	- Indicatore Temperatura CPU
	- Meteo
	- Radio++

- Applets già installate:
	- Cambio area di lavoro

- Estensioni:
	- Opacizza finestre
	- Pannelli trasparenti (attivo solo per versione scura)
	- Horizontal OSD

- Applet Calendario:
Utilizza un formato della data personalizzato: ATTIVO
Formato della data: %a %d %b | %H:%M
Formato della data per il tooltip: %A %d %B, %H:%M

CORREZIONE: la scorciatoia Super + C impostata per avviare / arrestare Conky va in conflitto con la scorciatoia di default per l'applet Calendario che è sempre Super + C. Cambiare in Alt + C.

- Applet Radio++:
Al primo click far installare mpv

NOTA BENE: ripristinare le impostazioni che si trovano nei files:
~/Scaricati/lm-21.3-cinnamon-elemento/applets-confs
~/Scaricati/lm-21.3-cinnamon-elemento/extensions-confs

- Ordine delle applets nel pannello:

Zona sinistra:
1. Menù (menu@cinnamon.org)
2. Elenco finestre raggruppate (grouped-window-list)

Zona centrale:
3. Indicatore temperatura CPU (su PC fisico)

Zona destra:
4. Radio++ (radio@driglu4it)
5. Update Manager (no applet, solo icona systray della app)
6. Unità rimovibili (removable-drives)
7. Gestione reti (network)
8. Audio (sound)
9. GPaste Reloaded
10. Meteo (weather@mockturtl)
11. Calendario (calendar@cinnamon.org)
12. Command Launcher (che avvia menu di rofi)
13. Gestore Sessione (sessionManager)

Togli l'applet Mostra il desktop

