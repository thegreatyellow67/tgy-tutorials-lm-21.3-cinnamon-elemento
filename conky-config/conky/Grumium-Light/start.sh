#!/bin/bash

killall conky
sleep 2s
		
conky -c $HOME/.config/conky/Grumium-Light/Grumium-Light.conf &> /dev/null &
